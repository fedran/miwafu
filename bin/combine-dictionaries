#!/usr/bin/env node

const fs = require("fs-extra");
const path = require("path");
const stringify = require("json-stable-stringify");
const yaml = require("js-yaml");

// Create the combined dictionary file.
let dict = [];
let syllableDict = {};

// Create a chain of actions inside an async method.
run();

async function run()
{
  // Get all the syllables (directory names).
  var syllables = await fs.readdir("src/dictionary");

  for (const syllable of syllables)
  {
    // Go through the dictionary files in this list.
    const syllablePath = path.join("src/dictionary", syllable);
    const files = await fs.readdir(syllablePath);
    const yamlFiles = files.filter(f => f.match(/\.yaml$/));

    for (const file of yamlFiles)
    {
      // Load the file into memory.
      const filePath = path.join(syllablePath, file);
      const buffer = await fs.readFile(filePath);
      const entry = yaml.safeLoad(buffer);

      entry.firstSyllable = syllable;

      // Make sure the syllable object exists.
      if (!syllableDict[syllable])
      {
        syllableDict[syllable] = [];
      }

      // Add them to the given lists.
      dict.push(entry);
      syllableDict[syllable].push(entry);
    }
  }

  // Make sure the directories exists.
  await fs.mkdir("dist").catch(() => {});
  await fs.mkdir("dist/dictionary").catch(() => {});

  // Write out all the files.
  const options = { sortKeys: true, space: "  " };
  fs.writeFile("dist/dictionary.json", stringify(dict, options));
  fs.writeFile("dist/dictionary.yaml", yaml.safeDump(dict, options));

  for (const syllable in syllableDict)
  {
    if (syllableDict.hasOwnProperty(syllable))
    {
      fs.writeFile(
        "dist/dictionary/" + syllable + ".json",
        stringify(syllableDict[syllable], options));
      fs.writeFile(
        "dist/dictionary/" + syllable + ".yaml",
        yaml.safeDump(syllableDict[syllable], options));
    }
  }
}
